package com.example.checkins.Utils

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.checkins.Interfaces.UbicacionListener
import com.example.checkins.Mensajes.Errores
import com.example.checkins.Mensajes.Mensaje
import com.example.checkins.Mensajes.Mensajes
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices.getFusedLocationProviderClient

class Ubicacion(var activity: AppCompatActivity, ubicacionListener: UbicacionListener) {
    private val permisoFineLocation = android.Manifest.permission.ACCESS_FINE_LOCATION
    private val permisoCoarseLocation = android.Manifest.permission.ACCESS_COARSE_LOCATION
    private val CODIGO_SOLICITUD_UBICACION = 100
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var callback:LocationCallback? = null

    init {
        fusedLocationClient = getFusedLocationProviderClient(activity.applicationContext)

        inicializarLocationRequest()

        callback = object: LocationCallback(){
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                ubicacionListener.ubicacionResponse(locationResult!!)
            }
        }
    }

    private fun inicializarLocationRequest()
    {
        locationRequest = LocationRequest.create()
        locationRequest?.interval = 10000
        locationRequest?.fastestInterval = 5000
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

    }

    private fun validarPermisosUbicacion():Boolean{
        val hayUbicacionPrecisa = ActivityCompat.checkSelfPermission(activity.applicationContext, permisoFineLocation) ==  PackageManager.PERMISSION_GRANTED
        val hayUbicacionOrdinaria = ActivityCompat.checkSelfPermission(activity.applicationContext, permisoCoarseLocation) ==  PackageManager.PERMISSION_GRANTED

        return  hayUbicacionPrecisa && hayUbicacionOrdinaria
    }

    private fun pedirPermisos(){
        val deboProveerContexto = ActivityCompat.shouldShowRequestPermissionRationale(activity, permisoFineLocation)

        if(deboProveerContexto){
            Mensaje.mensajeSuccess(activity.applicationContext, Mensajes.RATIONALE)
        }
        solicitudPermiso()
    }

    private fun solicitudPermiso(){
        ActivityCompat.requestPermissions(activity, arrayOf(permisoFineLocation, permisoCoarseLocation), CODIGO_SOLICITUD_UBICACION)
    }

    fun onRequestPermissionsResult(requestCode: Int, permission: Array<out String>, grantResult: IntArray){
        when(requestCode){
            CODIGO_SOLICITUD_UBICACION->{
                if(grantResult.size > 0 && grantResult[0] == PackageManager.PERMISSION_GRANTED){
                    obtenerUbicacion()
                }
                else
                {
                    Mensaje.mensajeError(activity.applicationContext, Errores.PERMISO_NEGADO)
                }
            }
        }
    }

    fun detenerActualizacionUbicacion()
    {
        this.fusedLocationClient?.removeLocationUpdates(callback)
    }
    fun inicializarUbicacion(){
        if(validarPermisosUbicacion()){
            obtenerUbicacion()
        }
        else
        {
            pedirPermisos()
        }
    }

    @SuppressLint("MissingPermission")
    private fun obtenerUbicacion() {
        validarPermisosUbicacion()
        fusedLocationClient?.requestLocationUpdates(locationRequest, callback, null)
    }
}