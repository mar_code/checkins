package com.example.checkins.Utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.checkins.Interfaces.HttpResponse
import com.example.checkins.Mensajes.Errores
import com.example.checkins.Mensajes.Mensaje

class Network(var activity: AppCompatActivity) {

    fun comprobarRed():Boolean{
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.activeNetwork
        } else { TODO("VERSION.SDK_INT < M") }
        val capabilities = connectivityManager.getNetworkCapabilities(networkInfo)
        return capabilities != null && (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || capabilities.hasTransport(
            NetworkCapabilities.TRANSPORT_CELLULAR))
    }

    fun httpRequest(context: Context, url: String, httpResponse: HttpResponse)
    {
        if(comprobarRed())
        {
            val queue = Volley.newRequestQueue(context)

            val solicitud = StringRequest(Request.Method.GET, url, Response.Listener<String>{
                    response ->
                        httpResponse.httpResponseSuccess(response)
            }, Response.ErrorListener {
                    error ->
                        Log.d("HTTP_REQUEST_ERROR", error.message.toString())
                        Mensaje.mensajeError(context, Errores.HTTP_ERROR)
            })
            queue.add(solicitud)
        }
        else{
            Mensaje.mensajeError(context, Errores.NO_HAY_RED)
        }

    }

    fun httpPOSTRequest(context: Context, url: String, httpResponse: HttpResponse)
    {
        if(comprobarRed())
        {
            val queue = Volley.newRequestQueue(context)

            val solicitud = StringRequest(Request.Method.POST, url, Response.Listener<String>{
                    response ->
                httpResponse.httpResponseSuccess(response)
            }, Response.ErrorListener {
                    error ->
                Log.d("HTTP_REQUEST_ERROR", error.message.toString())
                Mensaje.mensajeError(context, Errores.HTTP_ERROR)
            })
            queue.add(solicitud)
        }
        else{
            Mensaje.mensajeError(context, Errores.NO_HAY_RED)
        }
    }
}