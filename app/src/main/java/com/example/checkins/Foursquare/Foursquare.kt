package com.example.checkins.Foursquare

import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.checkins.Interfaces.CategoriasVenuesInterface
import com.example.checkins.Interfaces.HttpResponse
import com.example.checkins.Interfaces.ObtenerVenuesInterface
import com.example.checkins.Interfaces.UsuariosInterface
import com.example.checkins.Mensajes.Errores
import com.example.checkins.Mensajes.Mensaje
import com.example.checkins.Mensajes.Mensajes
import com.example.checkins.Utils.Network
import com.foursquare.android.nativeoauth.FoursquareOAuth
import com.google.gson.Gson

class Foursquare (var activity:AppCompatActivity, var activityDestino:AppCompatActivity){
    private val CODIGO_CONEXION = 200
    private val CODIGO_INTERCAMBIO_TOKEN = 201

    private val CLIENT_ID = "W3W0C0DUPKOS1BQUZTJJP0TRWHCTM5DDB2JQBOQYRA2ABDQ0"
    private val CLIENT_SECRET = "ONNJRSX1R1JQOPK5WXS3RR1R1SELR34D1LW1OT43IGR230CZ"

    private val SETTINGS =""
    private val ACCESS_TOKEN = "accessToken"

    private val URL_BASE = "https://api.foursquare.com/v2/"
    private val VERSION = "v=20200607"

    fun iniciarSesion()
    {
        val intent = FoursquareOAuth.getConnectIntent(activity.applicationContext, CLIENT_ID)

        if(FoursquareOAuth.isPlayStoreIntent(intent)) {
            Mensaje.mensajeError(
                activity.applicationContext,
                Errores.NO_HAY_APP_FSQR
            )
            activity.startActivity(intent)
        }
        else{
            activity.startActivityForResult(intent, CODIGO_CONEXION)
        }
    }

    fun validarActivityResult(requestCode:Int, resultCode:Int, data: Intent?){
        when(requestCode){
            CODIGO_CONEXION->{
                conexionCompleta(resultCode, data)
            }
            CODIGO_INTERCAMBIO_TOKEN->{
                intercambioTokenCompleta(resultCode, data)
            }
        }
    }

    private fun conexionCompleta(resultCode: Int, data: Intent?){
        val codigoRespuesta = FoursquareOAuth.getAuthCodeFromResult(resultCode, data)
        val excepcion = codigoRespuesta.exception

        if(excepcion == null)
        {
            val codigo = codigoRespuesta.code
            realizarIntercambioToken(codigo)
        }

        else{
            Mensaje.mensajeError(
                activity.applicationContext,
                Errores.ERROR_CONEXION_FSQR
            )
        }
    }

    private fun realizarIntercambioToken(codigo: String){
        val intent = FoursquareOAuth.getTokenExchangeIntent(activity.applicationContext, CLIENT_ID, CLIENT_SECRET, codigo)
        activity.startActivityForResult(intent, CODIGO_INTERCAMBIO_TOKEN)
    }

    private fun intercambioTokenCompleta(resultCode: Int, data: Intent?){
        val respuestaToken = FoursquareOAuth.getTokenFromResult(resultCode, data)
        val excepcion = respuestaToken.exception

        if(excepcion == null)
        {
            val accessToken = respuestaToken.accessToken
            if(!guardarToken(accessToken)){
                Mensaje.mensajeError(
                    activity.applicationContext,
                    Errores.ERROR_GUARDAR_TOKEN
                )
            }
            else{
                navegarSiguienteActividad()
            }
        }
        else{
            Mensaje.mensajeError(
                activity.applicationContext,
                Errores.ERROR_INTERCAMBIO_TOKEN
            )
        }
    }

    fun hayToken():Boolean{
        if(obtenerToken() == ""){
            return false
        }
        else{
            return true
        }
    }

    fun obtenerToken():String{
        val settings = activity.getSharedPreferences(SETTINGS, 0)
        val token = settings.getString(ACCESS_TOKEN, "")
        return token!!
    }

    private fun guardarToken(token: String):Boolean{
        if(token.isEmpty()){
            return false
        }

        val settings = activity.getSharedPreferences(SETTINGS, 0)
        val editor = settings.edit()

        editor.putString(ACCESS_TOKEN, token)

        editor.apply()
        return true
    }

    fun navegarSiguienteActividad(){
        activity.startActivity(Intent(this.activity, activityDestino::class.java))
        activity.finish()
    }

    fun obtenerVenues(lat:String, lon:String, obtenerVenuesInterface: ObtenerVenuesInterface) {
        val network = Network(activity)
        val seccion = "venues/"
        val metodo = "search"
        val ll = "ll=$lat,$lon"
        val url = "${URL_BASE+seccion+metodo}?$ll&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&$VERSION"
        network.httpRequest(activity.applicationContext, url, object:
            HttpResponse {
            override fun httpResponseSuccess(response: String) {
                var gson = Gson()
                var objetoRespuesta = gson.fromJson(response, FoursquareAPIRequestVenues::class.java)

                var meta = objetoRespuesta.meta
                var venues = objetoRespuesta.response?.venues

                if(meta?.code == 200) {
                    obtenerVenuesInterface.venuesGenerados(venues!!)
                }
                else {
                    if(meta?.code == 400) {
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            meta.errorDetail
                        )
                    }
                    else{
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            Errores.ERROR_QUERY
                        )
                    }
                }
            }
        })
    }

    fun obtenerVenues(lat:String, lon:String, categoryId:String, obtenerVenuesInterface: ObtenerVenuesInterface) {
        val network = Network(activity)
        val seccion = "venues/"
        val metodo = "search"
        val ll = "ll=$lat,$lon"
        val categoria = "categoryId=$categoryId"
        val url = "${URL_BASE+seccion+metodo}?$ll&$categoria&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&$VERSION"
        network.httpRequest(activity.applicationContext, url, object:
            HttpResponse {
            override fun httpResponseSuccess(response: String) {
                var gson = Gson()
                var objetoRespuesta = gson.fromJson(response, FoursquareAPIRequestVenues::class.java)

                var meta = objetoRespuesta.meta
                var venues = objetoRespuesta.response?.venues

                if(meta?.code == 200) {
                    obtenerVenuesInterface.venuesGenerados(venues!!)
                }
                else {
                    if(meta?.code == 400) {
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            meta.errorDetail
                        )
                    }
                    else{
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            Errores.ERROR_QUERY
                        )
                    }
                }
            }
        })
    }

    fun nuevoCheckin(id:String, location: Location, mensaje:String){
        val network = Network(activity)
        val token = "oauth_token= "+obtenerToken()
        val seccion = "checkins/"
        val metodo = "add"
        val query = "?venueId=" + id + "&shout=" + mensaje + "&ll=" + location.lat.toString() + "," + location.lng.toString() + "&" + token +"&" + VERSION
        val url = URL_BASE + seccion + metodo + query

        network.httpPOSTRequest(activity.applicationContext, url, object:
            HttpResponse {
            override fun httpResponseSuccess(response: String) {
                Log.d("RESPONSE", response)
                val gson = Gson()
                val objetoRespuesta = gson.fromJson(response, FoursquareAPInuevoCheckin::class.java)

                var meta = objetoRespuesta.meta
                if(meta?.code == 200) {
                    Mensaje.mensajeSuccess(activity.applicationContext, Mensajes.CHECKIN_SUCCESS)
                }
                else {
                    if(meta?.code == 400) {
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            meta.errorDetail
                        )
                    }
                    else{
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            Errores.ERROR_QUERY
                        )
                    }
                }
            }
        })
    }

    fun nuevoLike(id:String){
        val network = Network(activity)
        val token = "oauth_token= "+obtenerToken()
        val seccion = "checkins/"
        val metodo = "add"
        val query = "?venueId=" + id + "&shout=" + mensaje + "&ll=" + location.lat.toString() + "," + location.lng.toString() + "&" + token +"&" + VERSION
        val url = URL_BASE + seccion + metodo + query

        network.httpPOSTRequest(activity.applicationContext, url, object:
            HttpResponse {
            override fun httpResponseSuccess(response: String) {
                Log.d("RESPONSE", response)
                val gson = Gson()
                val objetoRespuesta = gson.fromJson(response, FoursquareAPInuevoCheckin::class.java)

                var meta = objetoRespuesta.meta
                if(meta?.code == 200) {
                    Mensaje.mensajeSuccess(activity.applicationContext, Mensajes.CHECKIN_SUCCESS)
                }
                else {
                    if(meta?.code == 400) {
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            meta.errorDetail
                        )
                    }
                    else{
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            Errores.ERROR_QUERY
                        )
                    }
                }
            }
        })
    }

    fun obtenerUsuarioActual(usuarioActualInterface: UsuariosInterface){
        val network = Network(activity)
        val token = "oauth_token= "+obtenerToken()
        val seccion = "users/"
        val metodo = "self"
        val query = "?$token&$VERSION"
        val url = URL_BASE + seccion + metodo + query

        network.httpRequest(activity.applicationContext, url, object:
            HttpResponse {
            override fun httpResponseSuccess(response: String) {
                val gson = Gson()
                val objetoRespuesta = gson.fromJson(response, FoursquareAPISelfUser::class.java)

                var meta = objetoRespuesta.meta
                if(meta?.code == 200) {
                    usuarioActualInterface.obtenerUsuarioActual(objetoRespuesta.response?.user!!)
                }
                else {
                    if(meta?.code == 400) {
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            meta.errorDetail
                        )
                    }
                    else{
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            Errores.ERROR_QUERY
                        )
                    }
                }
            }
        })
    }

    fun cargarCategorias(categoriasInterface: CategoriasVenuesInterface){
        val network = Network(activity)
        val token = "oauth_token= "+obtenerToken()
        val seccion = "venues/"
        val metodo = "categories"
        val query = "?$token&$VERSION"
        val url = URL_BASE + seccion + metodo + query

        network.httpRequest(activity.applicationContext, url, object:
            HttpResponse {
            override fun httpResponseSuccess(response: String) {
                val gson = Gson()
                val objetoRespuesta = gson.fromJson(response, FoursquareAPICategorias::class.java)

                var meta = objetoRespuesta.meta
                if(meta?.code == 200) {
                    categoriasInterface.categoriasVenues(objetoRespuesta.response?.categories!!)
                }
                else {
                    if(meta?.code == 400) {
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            meta.errorDetail
                        )
                    }
                    else{
                        Mensaje.mensajeError(
                            activity.applicationContext,
                            Errores.ERROR_QUERY
                        )
                    }
                }
            }
        })
    }
}