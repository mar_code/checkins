package com.example.checkins.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.checkins.Foursquare.Category
import com.example.checkins.Foursquare.Foursquare
import com.example.checkins.Interfaces.CategoriasVenuesInterface
import com.example.checkins.R
import com.example.checkins.RecyclerViewCategorias.AdaptadorCustom
import com.example.checkins.RecyclerViewCategorias.ClickListener
import com.example.checkins.RecyclerViewCategorias.LongClickListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_categorias.*
import kotlinx.android.synthetic.main.activity_pantalla_principal.*

class CategoriasActivity : AppCompatActivity() {

    var adaptador: AdaptadorCustom? = null
    var layoutManager: RecyclerView.LayoutManager? = null
    companion object{
        val VENUE_CATEGORIA = "checkins.CategoriasActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categorias)

        initToolbar()
        initRecyclerView()

        val fsqr = Foursquare(this, CategoriasActivity())
        fsqr.cargarCategorias(object: CategoriasVenuesInterface{
            override fun categoriasVenues(categorias: ArrayList<Category>) {
                Log.d("TEST_CAT", categorias.count().toString())

                implementacionRecyclerView(categorias)
            }
        })
    }

    private fun initRecyclerView(){
        rvCategorias?.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        rvCategorias?.layoutManager = layoutManager
    }

    private fun implementacionRecyclerView(categorias : ArrayList<Category>)
    {
        adaptador = AdaptadorCustom(categorias, object: ClickListener {
            override fun onClick(vista: View, index: Int) {
                val categoriaToJson = Gson()
                val categoriaActual = categoriaToJson.toJson(categorias.get(index))
                val intent = Intent(this@CategoriasActivity, VenuesPorCategoriaActivity::class.java)
                intent.putExtra(VENUE_CATEGORIA, categoriaActual)
                startActivity(intent)
            }
        }, object: LongClickListener {
            override fun longClick(vista: View, index: Int) {}
        })

        rvCategorias?.adapter = adaptador
    }

    fun initToolbar() {
        setTitle("Categorias")
        setSupportActionBar(toolbar_categorias)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar_categorias.setNavigationOnClickListener {
            finish()
        }
    }
}