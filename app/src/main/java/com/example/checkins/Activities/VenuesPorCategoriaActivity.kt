package com.example.checkins.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.checkins.Foursquare.Category
import com.example.checkins.Foursquare.Foursquare
import com.example.checkins.Foursquare.Venue
import com.example.checkins.Interfaces.ObtenerVenuesInterface
import com.example.checkins.Interfaces.UbicacionListener
import com.example.checkins.R
import com.example.checkins.RecyclerViewPrincipal.AdaptadorCustom
import com.example.checkins.RecyclerViewPrincipal.ClickListener
import com.example.checkins.RecyclerViewPrincipal.LongClickListener
import com.example.checkins.Utils.Ubicacion
import com.google.android.gms.location.LocationResult
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_categorias.*
import kotlinx.android.synthetic.main.activity_pantalla_principal.*
import kotlinx.android.synthetic.main.activity_venues_por_categoria.*

class VenuesPorCategoriaActivity : AppCompatActivity() {

    var adaptador: AdaptadorCustom? = null
    var layoutManager: RecyclerView.LayoutManager? = null
    var foursquare: Foursquare? = null
    var ubicacion: Ubicacion? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venues_por_categoria)

        val categoriaActualString = intent.getStringExtra(CategoriasActivity.VENUE_CATEGORIA)
        val gson = Gson()
        val categoriaActual = gson.fromJson(categoriaActualString, Category::class.java)

        foursquare = Foursquare(this, this)
        rvLugaresCategoria?.setHasFixedSize(true)
        initToolbar(categoriaActual.name)
        layoutManager = LinearLayoutManager(this)
        rvLugaresCategoria?.layoutManager = layoutManager

        if(foursquare?.hayToken()!!)
        {
            ubicacion = Ubicacion(this, object :
                UbicacionListener {
                override fun ubicacionResponse(locationResult: LocationResult) {
                    val lat = locationResult.lastLocation.latitude.toString()
                    val lon = locationResult.lastLocation.longitude.toString()
                    val categoryID = categoriaActual.id

                    foursquare?.obtenerVenues(lat, lon, categoryID, object :
                        ObtenerVenuesInterface {
                        override fun venuesGenerados(venues: ArrayList<Venue>) {
                            implementacionRecyclerView(venues)
                            for (venue in venues) {
                                Log.d("VENUE", venue.name)
                            }
                        }
                    })
                }
            })
        }
    }

    fun initToolbar(categoria: String) {
        setTitle(categoria)
        setSupportActionBar(toolbar_venues_categoria)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar_venues_categoria.setNavigationOnClickListener {
            finish()
        }
    }

    private fun implementacionRecyclerView(lugares : ArrayList<Venue>)
    {
        adaptador = AdaptadorCustom(lugares, object : ClickListener {
                override fun onClick(vista: View, index: Int) {
                   val venueToJson = Gson()
                    val venueActualString = venueToJson.toJson(lugares.get(index))
                    val intent = Intent(this@VenuesPorCategoriaActivity, DetallesVenue::class.java)
                    intent.putExtra(PantallaPrincipal.VENUE_ACTUAL, venueActualString)
                    startActivity(intent)
                }
            },
            object : LongClickListener {
                override fun longClick(vista: View, index: Int) {}
            })

        rvLugaresCategoria?.adapter = adaptador
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        ubicacion?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onStart() {
        super.onStart()
        ubicacion?.inicializarUbicacion()
    }

    override fun onPause() {
        super.onPause()
        ubicacion?.detenerActualizacionUbicacion()
    }
}