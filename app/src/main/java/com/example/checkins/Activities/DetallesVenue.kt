package com.example.checkins.Activities

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.checkins.Foursquare.Foursquare
import com.example.checkins.Interfaces.UsuariosInterface
import com.example.checkins.R
import com.example.checkins.Foursquare.User
import com.example.checkins.Foursquare.Venue
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detalles_venue.*
import kotlinx.android.synthetic.main.activity_venues_por_categoria.*
import java.net.URLEncoder

class DetallesVenue : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles_venue)
        val venueActualString = intent.getStringExtra(PantallaPrincipal.VENUE_ACTUAL)
        val gson = Gson()
        val venueActual = gson.fromJson(venueActualString, Venue::class.java)
        //Log.d("VENUEACTUAL", venueActual.name)

        initToolbar(venueActual.name)

        tvNombre.text = venueActual.name
        tvState.text = venueActual.location?.state
        tvCountry.text = venueActual.location?.country
        tvCategory.text = venueActual.categories?.get(0)?.name
        tvCheckins.text = venueActual.stats?.checkinsCount.toString()
        tvUsers.text = venueActual.stats?.usersCount.toString()
        tvTips.text = venueActual.stats?.tipCount.toString()

        val foursquare = Foursquare(
            this,
            DetallesVenue()
        )

        bCheckin.setOnClickListener {
            if(foursquare.hayToken()) {
                //foursquare.nuevoCheckin(venueActual.id, venueActual.location!!, "Hola")
                //Toast.makeText(this,"Hola", Toast.LENGTH_SHORT).show()
                val etMensaje = EditText(this)
                etMensaje.hint = "Hola"

                AlertDialog.Builder(this)
                    .setTitle("Nuevo Check-in")
                    .setMessage("Ingresa un mensaje")
                    .setView(etMensaje)
                    .setNegativeButton("Cancelar") { _, _ ->
                        
                    }
                    .setPositiveButton("Check-in") { _, _ ->
                        val mensaje = URLEncoder.encode(etMensaje.text.toString(), "UTF-8")
                        foursquare.nuevoCheckin(venueActual.id, venueActual.location!!, mensaje)
                    }
                    .show()
            }
        }

        bLike.setOnClickListener {

        }
    }

    fun initToolbar(categoria: String) {
        setTitle(categoria)
        setSupportActionBar(toolbar_detalles_venues)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar_detalles_venues.setNavigationOnClickListener {
            finish()
        }
    }
}