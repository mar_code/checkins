package com.example.checkins.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.ShareActionProvider
import androidx.core.view.MenuItemCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.checkins.*
import com.example.checkins.Foursquare.Foursquare
import com.example.checkins.Foursquare.Venue
import com.example.checkins.Interfaces.ObtenerVenuesInterface
import com.example.checkins.Interfaces.UbicacionListener
import com.example.checkins.RecyclerViewPrincipal.AdaptadorCustom
import com.example.checkins.RecyclerViewPrincipal.ClickListener
import com.example.checkins.RecyclerViewPrincipal.LongClickListener
import com.example.checkins.Utils.Ubicacion
import com.google.android.gms.location.LocationResult
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_pantalla_principal.*

class PantallaPrincipal : AppCompatActivity() {

    var ubicacion: Ubicacion? = null
    var foursquare: Foursquare? = null
    var adaptador:AdaptadorCustom? = null
    var layoutManager: RecyclerView.LayoutManager? = null
    companion object{
        val VENUE_ACTUAL = "checkins.PantallaPrincipal"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pantalla_principal)

        initToolbar()

        rvLugares?.setHasFixedSize(true)

        foursquare = Foursquare(this, this)

        layoutManager = LinearLayoutManager(this)
        rvLugares?.layoutManager = layoutManager

        if(foursquare?.hayToken()!!)
        {
            ubicacion = Ubicacion(this, object :
                UbicacionListener {
                override fun ubicacionResponse(locationResult: LocationResult) {
                    val lat = locationResult.lastLocation.latitude.toString()
                    val lon = locationResult.lastLocation.longitude.toString()

                    foursquare?.obtenerVenues(lat, lon, object :
                        ObtenerVenuesInterface {
                        override fun venuesGenerados(venues: ArrayList<Venue>) {
                            implementacionRecyclerView(venues)
                            for (venue in venues) {
                                //Log.d("VENUE", venue.name)
                            }
                        }
                    })
                }
            })
        }
    }

    private fun implementacionRecyclerView(lugares : ArrayList<Venue>)
    {
        adaptador = AdaptadorCustom(lugares, object: ClickListener {
            override fun onClick(vista: View, index: Int) {
                //Toast.makeText(this@PantallaPrincipal, lugares.get(index).name, Toast.LENGTH_SHORT).show()
                val venueToJson = Gson()
                val venueActualString = venueToJson.toJson(lugares.get(index))
                val intent = Intent(this@PantallaPrincipal, DetallesVenue::class.java)
                intent.putExtra(VENUE_ACTUAL, venueActualString)
                startActivity(intent)
            }
        }, object: LongClickListener {
            override fun longClick(vista: View, index: Int) {
                /*if (!isActionMode)
                {
                    startSupportActionMode(callback)
                    isActionMode = true
                    adaptador?.seleccionarItem(index)
                }
                else
                {
                    //hacer selecciones o deselecciones
                    adaptador?.seleccionarItem(index)
                }

                actionMode?.title = adaptador?.obtenerNumerosElementosSeleccionados().toString() + " seleccionados"*/
            }
        })

        rvLugares?.adapter = adaptador
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        ubicacion?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun initToolbar() {
        setTitle(R.string.app_name)
        setSupportActionBar(toolbar_principal)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_principal, menu)

        val itemCategory = menu?.findItem(R.id.icCategorias)
        val itemFavorite = menu?.findItem(R.id.icFav)
        val itemProfile = menu?.findItem(R.id.icPerfil)
        val itemLogout = menu?.findItem(R.id.icLogout)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId)
        {
            R.id.icCategorias -> {
                val intent = Intent(this, CategoriasActivity::class.java)
                startActivity(intent)
                return true
            }

            else -> {return super.onOptionsItemSelected(item)}
        }
    }

    override fun onStart() {
        super.onStart()
        ubicacion?.inicializarUbicacion()
    }

    override fun onPause() {
        super.onPause()
        ubicacion?.detenerActualizacionUbicacion()
    }
}