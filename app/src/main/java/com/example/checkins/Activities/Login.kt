package com.example.checkins.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.checkins.Foursquare.Foursquare
import com.example.checkins.R
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    var foursquare: Foursquare? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        foursquare = Foursquare(
            this,
            PantallaPrincipal()
        )

        if(foursquare!!.hayToken())
        {
            foursquare?.navegarSiguienteActividad()
        }

        bLogin.setOnClickListener {
            foursquare?.iniciarSesion()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        foursquare?.validarActivityResult(requestCode, resultCode, data)
    }
}